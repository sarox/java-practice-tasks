<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">
        <img src="/frontend/img/books.svg" width="30" height="30" class="d-inline-block align-top" alt="">
        Online library
    </a>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item ${param.activeBooks}">
            <a class="nav-link" href="/books/list">Books </a>
            </li>
            <li class="nav-item ${param.activeAdd}">
            <a class="nav-link" href="/books/new">Add </a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="/logout">Logout </a>
            </li>
        </ul>
    </div>
</nav>