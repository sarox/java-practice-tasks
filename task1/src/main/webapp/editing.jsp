<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://www.springframework.org/tags/form" prefix = "form" %>
<html>
<head>
    <title>Book editing</title>
    <%@ include file="/frontend/snippets/links.html" %>
</head>
<body>
    <c:import url="/frontend/snippets/header.jsp">
        <c:param name="activeBooks" value=""/>
        <c:param name="activeAdd" value="active"/>
    </c:import>

    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <form:form modelAttribute="book" action="/books/${action}" method="post">
                    <div class="form-group">
                        <div class="form-group">
                            <form:label path="name" for="name">Name</form:label>
                            <form:input path="name" type="text" name="name" value="${book.name}" class="form-control" id="name" />
                        </div>
            
                        <div class="form-group">
                            <form:label path="edition" for="edition">Edition</form:label>
                            <form:input path="edition" type="text" name="edition" value="${book.edition}" class="form-control" id="edition" />
                        </div>

                        <div class="form-group">
                            <form:label path="creationDate" for="creationDate">Creation date</form:label>
                            <form:input path="creationDate" type="text" name="creationDate" value="${book.creationDate}" class="form-control" id="creationDate" />
                        </div>
            
                        <div class="form-row">
                            <div class="col">
                                <form:label path="author.firstName" for="firstName">Author first name</form:label>
                                <form:input path="author.firstName" type="text" name="firstName" value="${book.author.firstName}" class="form-control" id="firstName" />
                            </div>
                            <div class="col">
                                <form:label path="author.lastName" for="lastName">Author last name</form:label>
                                <form:input path="author.lastName" type="text" name="lastName" value="${book.author.lastName}" class="form-control" id="lastName" />
                            </div>
                        </div>

                        <div class="form-group">
                            <form:label path="publisher" for="publisher">Publisher</form:label>
                            <form:input path="publisher" type="text" name="publisher" value="${book.publisher}" class="form-control" id="publisher" />
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary" value="Submit">
                </form:form>
            </div>
        </div>
    </div>
    
    <%@ include file="/frontend/snippets/scripts.html" %>
</body>
</html>