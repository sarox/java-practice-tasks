<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="/frontend/styles/login.css">
    <%@ include file="/frontend/snippets/links.html" %>
</head>
<body class="text-center">
    <form action="/login" method="post" name="login-form">
        <img class="mb-4" src="/frontend/img/books.svg" alt="" width="72" height="72">
        <h1 class="h3 mb-3 font-weight-normal">${message}</h1>
        <div class="form-group">
            <label for="username">Username</label>
            <input name="username" type="text" class="form-control" id="login" placeholder="Enter username" />
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input name="password" type="password" class="form-control" id="password" placeholder="Enter password" />
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <%@ include file="/frontend/snippets/scripts.html" %>
</body>
</html>