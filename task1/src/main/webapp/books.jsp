<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>List of books</title>
    <%@ include file="/frontend/snippets/links.html" %>
</head>
<body>
    <c:import url="/frontend/snippets/header.jsp">
        <c:param name="activeBooks" value="active"/>
        <c:param name="activeAdd" value=""/>
    </c:import>

    <div class="container">
        <div class="row">
            <div class="col-sm-8 offset-sm-2">
                <table class="table">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Edition</th>
                            <th scope="col">Creation date</th>
                            <th scope="col">Author</th>
                            <th scope="col">Publisher</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${books}" var="book">
                            <tr>
                                <td>${book.name}</td>
                                <td>${book.edition}</td>
                                <td>${book.creationDate}</td>
                                <td>${book.author.firstName} ${book.author.lastName}</td>
                                <td>${book.publisher}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <%@ include file="/frontend/snippets/scripts.html" %>
</body>
</html>
