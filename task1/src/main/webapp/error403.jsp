<%@ page contentType="text/html;charset=UTF-8" isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>List of books</title>
    <%@ include file="/frontend/snippets/links.html" %>
</head>
<body>
    <c:import url="/frontend/snippets/header.jsp">
        <c:param name="activeBooks" value=""/>
        <c:param name="activeAdd" value=""/>
    </c:import>

    <div class="text-center">
        <h2>Access denied!</h2>
    </div>

    <%@ include file="/frontend/snippets/scripts.html" %>
</body>
</html>
