<%@ page contentType="text/html;charset=UTF-8" isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>List of books</title>
    <%@ include file="/frontend/snippets/links.html" %>
</head>
<body>
    <c:import url="/frontend/snippets/header.jsp">
        <c:param name="activeBooks" value=""/>
        <c:param name="activeAdd" value=""/>
    </c:import>
    
    <h2>Something goes wrong</h2>
    <div>
        ${pageContext.exception.message}
    </div>

    <%@ include file="/frontend/snippets/scripts.html" %>
</body>
</html>
