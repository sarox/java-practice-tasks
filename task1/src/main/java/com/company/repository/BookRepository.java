package com.company.repository;

import com.company.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {
    @Query("from Book book where book.author.firstName = :firstName and book.author.lastName = :lastName")
    List<Book> findByAuthor(@Param(value = "firstName") String firstName, @Param(value = "lastName") String lastName);
}
