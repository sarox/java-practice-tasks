package com.company.service.implementation;

import com.company.entity.Author;
import com.company.entity.Book;
import com.company.exception.NoSuchIdException;
import com.company.repository.BookRepository;
import com.company.service.AuthorService;
import com.company.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private AuthorService authorService;

    public BookServiceImpl() {
    }

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getAll() {
        return bookRepository.findAll();
    }

    @Override
    public List<Book> findByAuthor(Author author) {
        return bookRepository.findByAuthor(author.getFirstName(), author.getLastName());
    }

    @Override
    public Book findById(long id) throws NoSuchIdException {
        return bookRepository.findById(id).orElseThrow(() -> new NoSuchIdException());
    }

    @Override
    public Book add(Book book) {
        return save(book);
    }

    @Override
    public Book edit(Book book) throws NoSuchIdException {
        if (bookRepository.existsById(book.getId())) {
            return save(book);
        }
        throw new NoSuchIdException();
    }

    @Override
    public void remove(long id) throws NoSuchIdException {
        if (bookRepository.existsById(id)) {
            bookRepository.deleteById(id);
        }
        throw new NoSuchIdException();
    }

    private Book save(Book book) {
        Author author = book.getAuthor();
        author = authorService.findOne(author);
        book.setAuthor(author);
        authorService.save(author);
        return bookRepository.save(book);
    }
}
