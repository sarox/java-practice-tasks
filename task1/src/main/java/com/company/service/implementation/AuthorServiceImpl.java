package com.company.service.implementation;

import com.company.entity.Author;
import com.company.repository.AuthorRepository;
import com.company.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

@Service
public class AuthorServiceImpl implements AuthorService {
    @Autowired
    private AuthorRepository authorRepository;

    public AuthorServiceImpl() {}

    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public Author findOne(Author author) {
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnorePaths("id");
        return authorRepository.findOne(Example.of(author, matcher)).orElse(author);
    }

    @Override
    public void save(Author author) {
        authorRepository.save(author);
    }
}
