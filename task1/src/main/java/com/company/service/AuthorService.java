package com.company.service;

import com.company.entity.Author;

public interface AuthorService {
    Author findOne(Author author);

    void save(Author author);
}
