package com.company.service;

import com.company.entity.Author;
import com.company.entity.Book;

import java.util.List;

public interface BookService {

    List<Book> getAll();

    List<Book> findByAuthor(Author author);

    Book findById(long id);

    Book add(Book book);

    Book edit(Book book);

    void remove(long id);
}
