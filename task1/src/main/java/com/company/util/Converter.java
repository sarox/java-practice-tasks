package com.company.util;

import com.company.dto.AuthorDto;
import com.company.dto.BookDto;
import com.company.entity.Author;
import com.company.entity.Book;

public class Converter {
    public static Author fromDtoToAuthor(AuthorDto authorDto) {
        Author author = new Author();
        author.setId(authorDto.getId());
        author.setFirstName(authorDto.getFirstName());
        author.setLastName(authorDto.getLastName());
        return author;
    }

    public static AuthorDto fromAuthorToDto(Author author) {
        AuthorDto authorDto = new AuthorDto();
        authorDto.setId(author.getId());
        authorDto.setFirstName(author.getFirstName());
        authorDto.setLastName(author.getLastName());
        return authorDto;
    }

    public static Book fromDtoToBook(BookDto bookDto) {
        Book book = new Book();
        book.setId(bookDto.getId());
        book.setName(bookDto.getName());
        book.setEdition(bookDto.getEdition());
        book.setCreationDate(bookDto.getCreationDate());
        book.setAuthor(fromDtoToAuthor(bookDto.getAuthor()));
        book.setPublisher(bookDto.getPublisher());
        return book;
    }

    public static BookDto fromBookToDto(Book book) {
        BookDto bookDto = new BookDto();
        bookDto.setId(book.getId());
        bookDto.setName(book.getName());
        bookDto.setEdition(book.getEdition());
        bookDto.setCreationDate(book.getCreationDate());
        bookDto.setAuthor(fromAuthorToDto(book.getAuthor()));
        bookDto.setPublisher(book.getPublisher());
        return bookDto;
    }
}
