package com.company.util;

import org.apache.log4j.Logger;

public class Validator {
    private static final Logger LOGGER = Logger.getLogger(Validator.class);

    public static boolean isEmpty(String param) {
        return param.trim().equals("");
    }

    public static boolean isEmpty(String... params) {
        for (String param : params) {
            if (isEmpty(param)) {
                return true;
            }
        }
        return false;
    }
}
