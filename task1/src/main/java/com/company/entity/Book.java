package com.company.entity;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "books")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private long id;
    @Column(nullable = false)
    private String name;
    private String edition;
    private Date creationDate;
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "author_id")
    private Author author;
    private String publisher;


    public Book() {
    }

    public Book(String name, String edition, Calendar creationDate, Author author, String publisher) {
        this.name = name;
        this.edition = edition;
        this.creationDate = creationDate.getTime();
        this.author = author;
        this.publisher = publisher;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return name + " (" + edition + ") " + getCreationDateFormatted() + ", by " + author + ", published by " + publisher;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else if (obj == this) {
            return true;
        } else if (!(obj instanceof Book)) {
            return false;
        }
        Book book = (Book) obj;
        return name.equals(book.getName()) && edition.equals(book.getEdition()) && creationDate.equals(book.getCreationDate()) && author.equals(book.getAuthor()) && publisher.equals(book.getPublisher());
    }

    @Override
    public int hashCode() {
        return name.hashCode() + edition.hashCode() + creationDate.hashCode() + author.hashCode() + publisher.hashCode();
    }

    private String getCreationDateFormatted() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return simpleDateFormat.format(creationDate);
    }
}
