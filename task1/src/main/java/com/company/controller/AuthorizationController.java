package com.company.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AuthorizationController {
    private static final Logger LOGGER = Logger.getLogger(AuthorizationController.class);
    private static final String LOGIN_PAGE = "login";

    @RequestMapping("/login")
    public String getLoginPage(@RequestParam(required = false) String error, ModelMap modelMap) {
        LOGGER.info("Method starts");
        String message = "Please, login in";
        if (error != null) {
            LOGGER.info("Invalid credentials");
            message = "Invalid login or password. Please try again";
        }
        LOGGER.debug("Message: " + message);
        modelMap.addAttribute("message", message);
        LOGGER.info("Method ends");
        return LOGIN_PAGE;
    }

    @RequestMapping("/logout")
    public String getLogoutPage(ModelMap modelMap) {
        LOGGER.info("Method starts");
        modelMap.addAttribute("message", "Successfully logged out");
        LOGGER.info("Method ends");
        return LOGIN_PAGE;
    }
}
