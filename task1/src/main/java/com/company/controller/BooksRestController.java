package com.company.controller;

import com.company.dto.BookDto;
import com.company.entity.Author;
import com.company.entity.Book;
import com.company.service.BookService;
import com.company.util.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.company.util.Converter.fromBookToDto;
import static com.company.util.Converter.fromDtoToBook;

@RestController
@RequestMapping("/api/v1/books")
public class BooksRestController {
    @Autowired
    private BookService bookService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<BookDto> getBooks() {
        List<Book> books = bookService.getAll();
        return books.stream()
                .map(Converter::fromBookToDto)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/{id:[0-9]+}", method = RequestMethod.GET)
    public BookDto getBook(@PathVariable long id) {
        Book book = bookService.findById(id);
        return fromBookToDto(book);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public BookDto createBook(@RequestBody BookDto bookDto) {
        Book book = fromDtoToBook(bookDto);
        book = bookService.add(book);
        return fromBookToDto(book);
    }

    @RequestMapping(value = "/{id:[0-9]+}", method = RequestMethod.PUT)
    public BookDto updateBook(@RequestBody BookDto bookDto) {
        Book book = fromDtoToBook(bookDto);
        book = bookService.edit(book);
        return fromBookToDto(book);
    }

    @RequestMapping(value = "/{id:[0-9]+}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteBook(@PathVariable long id) {
        bookService.remove(id);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public Collection<BookDto> getBooksByAuthor(@RequestParam(defaultValue = "") String firstName,
                                                @RequestParam(defaultValue = "") String lastName) {
        return bookService.findByAuthor(new Author(firstName, lastName)).stream()
                .map(Converter::fromBookToDto)
                .collect(Collectors.toList());
    }
}
