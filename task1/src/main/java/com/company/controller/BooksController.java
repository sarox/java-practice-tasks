package com.company.controller;

import com.company.entity.Author;
import com.company.entity.Book;
import com.company.exception.NoSuchIdException;
import com.company.service.AuthorService;
import com.company.service.BookService;
import com.company.util.Validator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/books")
public class BooksController {
    private static final Logger LOGGER = Logger.getLogger(BooksController.class);
    private static final String EDIT_PAGE = "editing";
    private static final String LIST_CONTROLLER = "forward:/books/list";
    private static final String BOOKS_PAGE = "books";
    @Autowired
    private BookService bookService;

    @RequestMapping("/list")
    public ModelAndView showBooks() {
        LOGGER.info("Method starts");
        List<Book> books = bookService.getAll();
        ModelAndView modelAndView = new ModelAndView("books");
        modelAndView.addObject("books", books);
        LOGGER.info("Method ends");
        return modelAndView;
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public ModelAndView getCreatingForm() {
        LOGGER.info("Method starts");
        ModelAndView modelAndView = new ModelAndView(EDIT_PAGE, "book", new Book());
        modelAndView.addObject("action", "new");
        LOGGER.info("Method ends");
        return modelAndView;
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String createBook(@ModelAttribute("book") Book book) {
        LOGGER.info("Method starts");
        if (Validator.isEmpty(book.getName())) {
            LOGGER.warn("Book name is empty");
            LOGGER.info("Method ends");
            return EDIT_PAGE;
        }
        bookService.add(book);
        LOGGER.info("Method ends");
        return LIST_CONTROLLER;
    }

    @RequestMapping(value = "/{id:[0-9]+}", method = RequestMethod.GET)
    public ModelAndView getEditingForm(@PathVariable long id) {
        LOGGER.info("Method starts");
        LOGGER.debug("Got id: " + id);
        String viewPage = EDIT_PAGE;
        ModelAndView modelAndView = new ModelAndView();
        try {
            Book book = bookService.findById(id);
            modelAndView.addObject("book", book);
            modelAndView.addObject("action", id);
        } catch (NoSuchIdException e) {
            LOGGER.error(e.getMessage());
            viewPage = "redirect:/books/new";
        }
        LOGGER.debug("View page: " + viewPage);
        modelAndView.setViewName(viewPage);
        LOGGER.info("Method ends");
        return modelAndView;
    }

    @RequestMapping(value = "/{id:[0-9]+}", method = RequestMethod.POST)
    public String editBook(@ModelAttribute("book") Book book) {
        LOGGER.info("Method starts");
        if (Validator.isEmpty(book.getName())) {
            LOGGER.warn("Book name is empty");
            LOGGER.info("Method ends");
            return EDIT_PAGE;
        }
        bookService.edit(book);
        LOGGER.info("Method ends");
        return LIST_CONTROLLER;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ModelAndView searchByAuthor(@RequestParam String firstName, @RequestParam String lastName) {
        LOGGER.info("Method starts");
        LOGGER.debug("Got firstName: " + firstName + ", lastName: " + lastName);
        List<Book> books = bookService.findByAuthor(new Author(firstName, lastName));
        ModelAndView modelAndView = new ModelAndView(BOOKS_PAGE);
        modelAndView.addObject("books", books);
        LOGGER.info("Method ends");
        return modelAndView;
    }
}
