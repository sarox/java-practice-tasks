package com.company.controller;

import com.company.entity.Author;
import com.company.entity.Book;
import com.company.exception.NoSuchIdException;
import com.company.service.BookService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

@RunWith(MockitoJUnitRunner.class)
public class BooksRestControllerTest {
    private static final String URL_PREFIX = "/api/v1/books";
    private List<Book> books;
    @Mock
    private BookService bookService;
    @InjectMocks
    private BooksRestController restController = new BooksRestController();

    @Before
    public void init() {
        standaloneSetup(restController);

        books = new LinkedList<>();
        Calendar creationDate = new GregorianCalendar(2018, 1, 1);
        Author author1 = new Author("FirstName1", "LastName1");
        Author author2 = new Author("FirstName2", "LastName2");
        Book book1 = new Book("Book1", "1st", creationDate, author1, "Publisher1");
        Book book2 = new Book("Book2", "1st", creationDate, author2, "Publisher1");
        Book book3 = new Book("Book3", "1st", creationDate, author2, "Publisher1");
        book1.setId(1);
        book2.setId(2);
        book3.setId(3);
        books.add(book1);
        books.add(book2);
        books.add(book3);
    }

    @Test
    public void getBooksTest() {
        final String url = URL_PREFIX;

        org.mockito.BDDMockito.given(bookService.getAll()).willReturn(books);
        given()
                .get(url)
                .then()
                .statusCode(200)
                .and()
                .body("", hasSize(books.size()))
                .body("get(0).name", equalTo(books.get(0).getName()))
                .body("get(1).name", equalTo(books.get(1).getName()))
                .body("get(2).name", equalTo(books.get(2).getName()));

        org.mockito.BDDMockito.given(bookService.getAll()).willReturn(new LinkedList<>());
        given()
                .get(url)
                .then()
                .statusCode(200)
                .and()
                .body("", hasSize(0));
    }

    @Test
    public void getBookTest() {
        final String url = URL_PREFIX + "/{id}";
        Calendar creationDate = new GregorianCalendar(2018, 1, 1);
        Book book = books.get(0);

        org.mockito.BDDMockito.given(bookService.findById(1)).willReturn(book);
        when().get(url, 1)
                .then()
                .statusCode(200)
                .and()
                .body("id", equalTo(1))
                .body("name", equalTo(book.getName()))
                .body("edition", equalTo(book.getEdition()))
                .body("creationDate", equalTo(creationDate.getTimeInMillis()))
                .body("author.firstName", equalTo(book.getAuthor().getFirstName()))
                .body("author.lastName", equalTo(book.getAuthor().getLastName()))
                .body("publisher", equalTo(book.getPublisher()));

        org.mockito.BDDMockito.given(bookService.findById(2)).willThrow(new NoSuchIdException());
        when().get(url, 2)
                .then()
                .statusCode(404);
    }

    @Test
    public void createBookTest() {
        final String url = URL_PREFIX;
        Calendar creationDate = new GregorianCalendar(2018, 1, 1);
        Book book = books.get(0);
        Book newBook = new Book("Book1", "1st", creationDate, new Author("FirstName1", "LastName1"), "Publisher1");

        org.mockito.BDDMockito.given(bookService.add(newBook)).willReturn(book);
        given()
                .contentType("application/json")
                .body(newBook)
                .when()
                .post(url)
                .then()
                .statusCode(201)
                .and()
                .body("id", equalTo(1))
                .body("name", equalTo(book.getName()))
                .body("edition", equalTo(book.getEdition()))
                .body("creationDate", equalTo(creationDate.getTimeInMillis()))
                .body("author.firstName", equalTo(book.getAuthor().getFirstName()))
                .body("author.lastName", equalTo(book.getAuthor().getLastName()))
                .body("publisher", equalTo(book.getPublisher()));
    }

    @Test
    public void updateBookTest() {
        final String url = URL_PREFIX + "/{id}";
        Calendar creationDate = new GregorianCalendar(2018, 1, 1);
        Book book = books.get(0);

        org.mockito.BDDMockito.given(bookService.edit(book)).willReturn(book);
        given()
                .contentType("application/json")
                .body(book)
                .when()
                .put(url, 1)
                .then()
                .statusCode(200)
                .and()
                .body("id", equalTo(1))
                .body("name", equalTo(book.getName()))
                .body("edition", equalTo(book.getEdition()))
                .body("creationDate", equalTo(creationDate.getTimeInMillis()))
                .body("author.firstName", equalTo(book.getAuthor().getFirstName()))
                .body("author.lastName", equalTo(book.getAuthor().getLastName()))
                .body("publisher", equalTo(book.getPublisher()));

        book.setId(0);
        org.mockito.BDDMockito.given(bookService.edit(book)).willThrow(new NoSuchIdException());
        given()
                .contentType("application/json")
                .body(book)
                .when()
                .put(url, 1)
                .then()
                .statusCode(404);

    }

    @Test
    public void deleteBookTest() {
        final String url = URL_PREFIX + "/{id}";
        when()
                .delete(url, 1)
                .then()
                .statusCode(200);
    }

    @Test
    public void getBooksByAuthor() {
        final String url = URL_PREFIX + "/search";
        Author author1 = new Author("FirstName1", "LastName1");
        Author author2 = new Author("FirstName2", "LastName2");
        List<Book> books1 = books.stream()
                .filter(book -> book.getAuthor().equals(author1))
                .collect(Collectors.toList());
        List<Book> books2 = books.stream()
                .filter(book -> book.getAuthor().equals(author2))
                .collect(Collectors.toList());

        org.mockito.BDDMockito.given(bookService.findByAuthor(author1)).willReturn(books1);
        given()
                .queryParam("firstName", author1.getFirstName())
                .queryParam("lastName", author1.getLastName())
                .when()
                .get(url)
                .then()
                .statusCode(200)
                .and()
                .body("", hasSize(books1.size()))
                .body("get(0).name", equalTo(books1.get(0).getName()))
                .body("get(0).author.firstName", equalTo(author1.getFirstName()))
                .body("get(0).author.lastName", equalTo(author1.getLastName()));

        org.mockito.BDDMockito.given(bookService.findByAuthor(author2)).willReturn(books2);
        given()
                .queryParam("firstName", author2.getFirstName())
                .queryParam("lastName", author2.getLastName())
                .when()
                .get(url)
                .then()
                .statusCode(200)
                .and()
                .body("", hasSize(books2.size()))
                .body("get(0).name", equalTo(books2.get(0).getName()))
                .body("get(0).author.firstName", equalTo(author2.getFirstName()))
                .body("get(0).author.lastName", equalTo(author2.getLastName()))
                .body("get(1).name", equalTo(books2.get(1).getName()))
                .body("get(1).author.firstName", equalTo(author2.getFirstName()))
                .body("get(1).author.lastName", equalTo(author2.getLastName()));
    }

    @After
    public void clear() {
        books.clear();
    }
}
