package com.company.service;

import com.company.entity.Author;
import com.company.entity.Book;
import com.company.repository.BookRepository;
import com.company.service.implementation.BookServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class BookServiceTest {
    private BookRepository bookRepository;
    private BookService bookService;
    private List<Book> books;

    @Before
    public void init() {
        bookRepository = mock(BookRepository.class);
        bookService = new BookServiceImpl(bookRepository);
        books = new LinkedList<>();
        Calendar creationDate = new GregorianCalendar(2018, 1, 1);
        Author author1 = new Author("First1", "Last1");
        Author author2 = new Author("First2", "Last2");
        books.add(new Book("Book1", "1st edition", creationDate, author1, "Publisher1"));
        books.add(new Book("Book2", "1st edition", creationDate, author1, "Publisher1"));
        books.add(new Book("Book3", "1st edition", creationDate, author2, "Publisher1"));
    }

    @Test
    public void findByAuthorTest() {
        Author author1 = new Author("First1", "Last1");
        Author author2 = new Author("First2", "Last2");
        List<Book> expected1 = books.stream()
                .filter(book -> book.getAuthor().equals(author1))
                .collect(Collectors.toList());
        List<Book> expected2 = books.stream()
                .filter(book -> book.getAuthor().equals(author2))
                .collect(Collectors.toList());

        given(this.bookRepository.findByAuthor(author1.getFirstName(), author1.getLastName())).willReturn(expected1);
        given(this.bookRepository.findByAuthor(author2.getFirstName(), author2.getLastName())).willReturn(expected2);

        List<Book> actual1 = this.bookService.findByAuthor(new Author("First1", "Last1"));
        List<Book> actual2 = this.bookService.findByAuthor(new Author("First2", "Last2"));

        assertEquals(expected1.size(), actual1.size());
        assertEquals(expected1, actual1);
        assertEquals(expected2.size(), actual2.size());
        assertEquals(expected2, actual2);
    }

    @Test
    public void findByIdTest() {
        long id = 1;
        Book expected = books.get(0);
        expected.setId(id);

        given(bookRepository.findById(id)).willReturn(Optional.of(expected));

        Book actual = this.bookService.findById(id);

        assertEquals(expected, actual);
    }

    @Test
    public void getAllTest() {
        List<Book> expected = new LinkedList<>(books);

        given(bookRepository.findAll()).willReturn(expected);

        List<Book> actual = bookService.getAll();

        assertEquals(expected.size(), actual.size());
        assertEquals(expected, actual);
    }

    @After
    public void clear() {
        books.clear();
    }
}
