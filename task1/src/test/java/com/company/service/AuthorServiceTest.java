package com.company.service;

import com.company.entity.Author;
import com.company.repository.AuthorRepository;
import com.company.service.implementation.AuthorServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class AuthorServiceTest {
    private AuthorRepository authorRepository;
    private AuthorService authorService;

    @Before
    public void init() {
        authorRepository = mock(AuthorRepository.class);
        authorService = new AuthorServiceImpl(authorRepository);
    }

    @Test
    public void findOneTest() {
        Author author = new Author("First1", "Last1");
        author.setId(1);
        Author expected1 = new Author("First1", "Last1");
        Author expected2 = new Author("First1", "Last1");
        expected1.setId(0);
        expected2.setId(1);

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnorePaths("id");
        given(authorRepository.findOne(Example.of(expected1, matcher))).willReturn(Optional.of(expected1));
        given(authorRepository.findOne(Example.of(expected2, matcher))).willReturn(Optional.of(author));

        Author actual1 = authorService.findOne(expected1);
        Author actual2 = authorService.findOne(expected2);

        assertEquals(expected1, actual1);
        assertEquals(expected2, actual2);
    }
}
